var data;
var index = 0;

function loadData() {
  $.ajax({
    url: "https://dps-fe-test-api.herokuapp.com/" + index,
    success: function(result) {
      data = result.data;
      injectData();
    }
  });
}

function injectData() {
  $("#data-title").text(data.title);
  $("#data-image").attr("src", data.image);
  $("#data-desc").text(data.description);
  reviewHtml = "";
  for (i = 0; i < data.reviews.length; i++) {
    reviewHtml +=
      '<div  class="media">' +
      '<img src="' +
      data.reviews[i].avatar +
      '" class="mr-3">' +
      '<div class="media-body">' +
      '<h5 class="mt-0">' +
      data.reviews[i].author +
      " - " +
      data.reviews[i].score +
      " Stars" +
      "</h5>" +
      data.reviews[i].description +
      "</div></div><br>";
  }

  $("#data-review").html(reviewHtml);

  $("#data-score").text(calcStars() + "/10 Stars");

  $("#data-experience").text(
    "If you have a few minutes, please share your experience at " + data.title
  );
}

function calcStars() {
  scores = 0;
  for (i = 0; i < data.reviews.length; i++) {
    scores += data.reviews[i].score;
  }
  return (scores / data.reviews.length).toFixed(1);
}

function changeIndex(current) {
  if (current != index || current == "rand") {
    index = current;
    loadData();
  }
}

function formValidate() {
  resetValidate();
  if ($("#reviewNickname").val() == "") {
    $("#nicknameRequired").show();
  } else if ($("#reviewScore").val() == "") {
    $("#scoreRequired").show();
  } else if ($("#reviewScore").val() < 0 || $("#reviewScore").val() > 10) {
    $("#scoreInvalid").show();
  } else {
    addReview();
  }
}

function resetValidate() {
  $("#nicknameRequired").hide();
  $("#scoreRequired").hide();
  $("#scoreInvalid").hide();
}

function addReview() {
  newReview =
    '<div  class="media">' +
    '<img src="https://i.pravatar.cc//100?" class="mr-3">' +
    '<div class="media-body">' +
    '<h5 class="mt-0">' +
    $("#reviewNickname").val() +
    " - " +
    $("#reviewScore").val() +
    " Stars" +
    "</h5>";

  if ($("#reviewDescription").val() != "") {
    newReview += $("#reviewDescription").val();
  } else {
    newReview += "No comment left";
  }

  newReview += "</div></div><br>";

  $("#data-review").append(newReview);
}

$(document).ready(function() {
  loadData();
});
